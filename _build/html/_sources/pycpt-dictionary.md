# The PyCPT Dictionary of subseasonal models

To make PyCPT "plug and play" with the subseasonal models of choice, several SubX 
and S2S models have been added to the PyCPT dictionary. However, not all models are 
compatible with each other.

## Which models are compatible?

- To make real-time forecasts, only SubX can be used
- To make a mulitmodel ensemble (aka "NextGen") forecast requires hindcasts initialized
on the same days; this makes ECCC not compatible with other SubX models

---

