# README #

Jupyterbook Users Guide for PyCPT S2S.

[Current rendered version of the book.](https://iri.columbia.edu/~awr/pycpt/html/intro.html)

### What is this repository for? ###

* An online Users Guide for using the subseasonal versioon of PyCPT v1.9.
* Built with [Jupyterbook.](https://jupyterbook.org/)
* Version 0.1, A. W. Robertson, June 2021.
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Clone the repo.
* Install [Jupyterbook](https://jupyterbook.org/).
* Build the book using " % jupyter-book build . "
* Follow instructions https://jupyterbook.org/intro.html

### Who do I talk to? ###

* Andrew Robertson, awr@iri.columbia.edu