% PyCPT for Subseasonal Forecasts

# Introduction

```{image} img/intro.png
:alt: fishy
:class: bg-primary
:width: 800px
:align: left
```

Welcome to PyCPT for Subseasonal Forecasts! The goal of this tutorial is to learn how to
 configure and run PyCPT in order to calibrate 
subseasonal forecasts from the [NOAA SubX](https://cpo.noaa.gov/Meet-the-Divisions/Earth-System-Science-and-Modeling/MAPP/Research-to-Operations-and-Applications/Subseasonal-Experiment)
 and [WWRP/WCRP S2S project](http://s2sprediction.net) databases, and eventually to construct multimodel forecasts.

We will focus on __SubX__ because the forecasts are available publicly in real time. 
The ECMWF model---which is part of the S2S database---will also be covered since it's part of
the [IRI ACToday project](https://iri.columbia.edu/actoday/) has access to the real-time forecasts as part of the 
[S2S Real Time Pilot Initiative](http://s2sprediction.net/xwiki/bin/view/dtbs/RealtimePilot/).

 __Who is this guide for?__

Basic familiarity with CPT and with using PyCPT for seasonal forecasting is assumed, as
is a basic familiarity with subseasonal forecasting.

% __How to use this tutorial__


%## Useful resources

---